# Cucumber Hello World

## About

This repository is a simple cucumber hello world example based on this [tutorial](https://cucumber.io/docs/guides/10-minute-tutorial/)

### Built With

* [VS Code](https://code.visualstudio.com/)

## Getting Started

To get a local copy up and running follow these simple steps:

### Prerequisites

* Node v14.16.0

### Installation

1. Clone the repo
    ```sh
    git clone https://bryancancel@bitbucket.org/bryancancel/cucumber-hello-world.git
    ```
2. Install NPM packages
    ```sh
    npm install
    ```

## Usage

3. Run Cucumber tests
   ```sh
   npm test
   ```

## Roadmap

See the [open issues](https://github.com/github_username/repo_name/issues) for a list of proposed features (and known issues)

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License



## Contact

Your Name - [@twitter_handle](https://twitter.com/twitter_handle) - email

Project Link: [https://github.com/github_username/repo_name](https://github.com/github_username/repo_name)

## Acknowledgements

* [Cucumber Tutorial](https://cucumber.io/docs/guides/10-minute-tutorial/)