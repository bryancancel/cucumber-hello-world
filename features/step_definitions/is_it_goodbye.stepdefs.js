const assert = require('assert');
const { Given, When, Then } = require('cucumber');

// we should move the isItFriday function out from the test code into production code
function isItGoodybe(today) {
    if (today === "yes") {
        return "Goodbye";
    } else {
        return "No";
    }
}

Given('today is today', function () {
    assert(true)
});

When('I ask whether it\'s time to say goodbye', function () {
    assert.equal(isItGoodybe("yes"), "Goodbye")
});

Then('I should be told Goodbye', function () {
    assert.equal(isItGoodybe("no"), "No")
});